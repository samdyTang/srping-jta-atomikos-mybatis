# srping-jta-atomikos-mybatis

#### 介绍
- 基于spring集成JTA-Atomikos支持在一个事务中同时操作多个数据源能够做到事务回滚效果。
- 使用spring-quartz定时执行相关业务。
- 支持数据源连接oracle11g、12C和mysql5.0+版本数据库。

#### 软件架构
spring、jta-atomikos、mybatis、log4j、slf4j、spring-quartz等


#### 安装教程

1. 将项目直接git到本机后，用idea编辑器打开该maven项目。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/145734_04e8dade_4840915.png "屏幕截图.png")

2. 找到项目jdbc.properties配置文件，分别配置oracle和mysql数据源相关连接信息。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/150016_4eb4e472_4840915.png "屏幕截图.png")

3. 在oracle和mysql的数据库中分别创建MEMBER和point表，sql文件放在docs目录。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/150500_5e030b91_4840915.png "屏幕截图.png")

#### 测试说明

1. 使用单元测类调用一个业务方法addMemberAndPoints进行数据增加操作，addMemberAndPoints方法中的java异常先注释，看是否能同时对Oracle和mysql数据库插入数据。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/152741_720d4af4_4840915.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/152845_b8115d65_4840915.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/151830_34d95483_4840915.png "屏幕截图.png")

2. 接着验证addMemberAndPoints执行报错过程，放开MemberServiceImpl中注释，再次运行单元测试类。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/152058_22286174_4840915.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/152228_696e1009_4840915.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/152344_f18cf172_4840915.png "屏幕截图.png")

由此得出结论，在同一个事务中执行多个数据源操作情况时，如果事务某一环节发生异常，那么都将会对两个数据源进行事务回滚操作。

3. 任务定时执行操作，设置执行间隔时间，可自行百度spring-quartz时间设置格式。

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/153125_465896c8_4840915.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0918/153230_57757a5f_4840915.png "屏幕截图.png")

4.tomcat启动项目，查看定时器执行效果。

#### QQ675681563

