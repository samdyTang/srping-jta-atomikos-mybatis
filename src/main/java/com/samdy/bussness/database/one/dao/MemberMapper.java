package com.samdy.bussness.database.one.dao;

import com.samdy.bussness.database.one.model.Member;
import org.springframework.stereotype.Repository;

@Repository("memberMapper")
public interface MemberMapper {
    void addMember(Member member);
}
