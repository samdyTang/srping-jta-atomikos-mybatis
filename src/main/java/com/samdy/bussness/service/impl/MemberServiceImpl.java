package com.samdy.bussness.service.impl;


import com.samdy.bussness.database.one.dao.MemberMapper;
import com.samdy.bussness.database.one.model.Member;
import com.samdy.bussness.database.two.dao.PointMapper;
import com.samdy.bussness.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by lisen on 2017/12/8.
 */
@Service("memberServiceWa")
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private PointMapper pointMapper;

    public void addMemberAndPoints(String name, int point) {
        Member member = new Member();
        member.setName(name);
        memberMapper.addMember(member);
       /* String str = null;
        str.equals("lisen");*/
        pointMapper.addPoint(point);


    }
}
