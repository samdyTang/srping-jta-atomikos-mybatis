package com.samdy.bussness.quartz;


import com.samdy.bussness.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description:数据库定时任务
 * @Auther: Samdy
 * @Date: 2019/9/9 13:36
 */
public class QuartzJobsForDatabase {
    private static final Logger log = LoggerFactory.getLogger(QuartzJobsForDatabase.class);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    @Qualifier("memberServiceWa")
    private MemberService memberServiceWa;

    public void executeA() {
        memberServiceWa.addMemberAndPoints("tangshuai",60);
        log.debug(sdf.format(new Date()) + "执行A任务");
    }
}
