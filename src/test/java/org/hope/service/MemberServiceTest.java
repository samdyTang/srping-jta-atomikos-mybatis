package org.hope.service;

import com.samdy.bussness.service.MemberService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by lisen on 2017/12/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring.xml"})
public class MemberServiceTest {

    @Autowired
    @Qualifier("memberServiceWa")
    private MemberService memberServiceWa;

    @Test
    public void addMemberAndPointTest() {
        String name = "马化腾";
        int points = 50;
        memberServiceWa.addMemberAndPoints(name, points);
    }
}
